# README #

--- VR Precision of mental representations, PILOT: scripts for analyses

Created and run in R version 3.6.3 (2020-02-29)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Manjaro Linux

### What is this repository for? ###

Data analysis scripts:
- *VR_pilot_analyse1_outlier*: outlier analyses and preparation of memory data
- *VR_pilot_analyse2_smmodel*: model fit

### How do I get set up? ###

* Scripts must be run in order analyse1 first and analyse2 second. 
* To run the scripts, you need a general folder containing a subfolder 'progsR' with the scripts and a subfolder 'inR' containing the raw data.
* More subfolders will be created during analyses.
